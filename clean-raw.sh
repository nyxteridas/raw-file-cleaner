#!/bin/bash

###################################################################
# Since raw images are very big, this script is used to 
#         clean your raw images in order to keep only those that 
#         you have already edited, and for that reason 
#         it's worth keeping them.
#
# The hierarchy of the folders is shown below:
#
#  - Folder with all photos
#  -- Edited_photo_1.jpg
#  -- Edited_photo_2.tiff
#  -- ...
#  -- Edited_photo_n.png
#  -- Folder with raw photos
#  --- raw_photo_1.extension
#  --- raw_photo_2.extension
#  --- ...
#  --- raw_photo_n.extension
#
# It doesn't matter in what extension the edited photos are saved. 
# What matters, is to keep the same filename between a raw and 
#       an edited image.
#
# The script first scans the edited images and keeps only 
#       their filename ignoring their extension.
# The second step is to check if this filename with the 
#       given raw extension exists in the 
#       folder where the raw files are kept (also given)
# The third step is to move those raw files in a temp folder
# The fourth step is to remove all the raw files not needed
# The fifth step is to return the raw files that will be kept 
#       to their original location and remove temp folder
###################################################################

echo -e "Please insert the absolute path to the folder you have your photos  (case sensitive):"
read FOLDER
echo -e "Please insert the name of the folder you have the raw files in (case sensitive):"
read RAW
echo -e "Please insert the file extension of the raw format you are using (case sensitive):"
read EXTENSION

cd "$FOLDER/$RAW"
mkdir tmp

for entry in "$FOLDER"/*
do
  if [ -f "$entry" ]; then
    filename=$(basename -- "$entry")    
    filename="${filename%.*}"
        
    if [ -e "$filename.$EXTENSION" ]; then
       mv "$filename.$EXTENSION" tmp/.
    fi
  fi
done

find . -maxdepth 1 -type f -delete
mv tmp/* .
rm -rf tmp


