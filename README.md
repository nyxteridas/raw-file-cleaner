# Raw file cleaner v1.0

Usually when working with photographs, you want to edit some of the RAW images that you have taken (most of the times those who are good enough), export them in JPEG or other format, and then erase all the RAW files that didn't worth keeping.

This bash script does this job for you: erases the raw files you haven't edited, assuming that you do not want them in your hard drive.

---

## How it works

1. The script takes as first input the absolute path to the directory in which you have exported the final images (for example:  /home/user/images/myphotos/).
2. The second input is the name of the directory in which you have the raw files. The script assumes that this folder is a subdirectory to the one you have exported the final imgaes (for example, based on the above example path, the path to the raw images should be something like: /home/user/images/myphotos/raw files. So, the input you should give is "raw files" without quotes).
3. The third input is the extension of your raw files (NEF for Nikon, CR2 for Canon etc). The input here is case sensitive.
4. Then the script does the comparison between the two directories and removes the raw files which have not match with the exported images. The script assumes you have kept the filenames of the exported images as they are in raw files.
